package com.iconcreations.balain.domain.di

import javax.inject.Scope

@Scope
annotation class DomainScope