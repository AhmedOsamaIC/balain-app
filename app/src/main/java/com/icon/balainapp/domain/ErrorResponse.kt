package com.iconcreations.balain.domain

import androidx.annotation.NonNull
import com.google.gson.annotations.SerializedName

class ErrorResponse {

    @SerializedName("message")
    @NonNull
    var errorMessage:String? = null

}