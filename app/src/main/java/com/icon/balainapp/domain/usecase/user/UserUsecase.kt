package com.iconcreations.balain.domain.usecase.user

import com.iconcreations.balain.data.remote.model.response.User

interface UserUsecase {

    fun setUser(user: User)
    fun getUser(): User
    fun clearUser(): Boolean
    fun changeLanguage(lang: String): Boolean
}