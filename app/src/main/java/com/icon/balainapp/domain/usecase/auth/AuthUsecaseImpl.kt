package com.iconcreations.balain.domain.usecase.auth

import com.iconcreations.balain.data.local.auth.AuthStore
import com.iconcreations.balain.data.repo.auth.AuthRepo
import javax.inject.Inject

class AuthUsecaseImpl @Inject constructor(private val authRepo: AuthRepo): AuthUsecase{

    override fun setAuth(auth: String): Boolean {
        return authRepo.setAuth(auth)
    }

    override fun removeAuth(): Boolean {
        return authRepo.clearAuth()
    }

    override fun getAuth(): String? {
        return authRepo.getAuth()
    }
}