package com.iconcreations.balain.domain.usecase.di

import com.iconcreations.balain.domain.usecase.auth.AuthUsecase
import com.iconcreations.balain.domain.usecase.auth.AuthUsecaseImpl
import com.iconcreations.balain.domain.usecase.user.UserUsecase
import com.iconcreations.balain.domain.usecase.user.UserUsecaseImpl
import dagger.Binds
import dagger.Module

@Module
interface UsecaseModule {

    @Binds
    fun bindsUserUsecase(userUsecaseImpl: UserUsecaseImpl): UserUsecase

    @Binds
    fun bindsAuthUsecase(authUsecaseImpl: AuthUsecaseImpl): AuthUsecase
}