package com.iconcreations.balain.domain.mappers

import androidx.annotation.NonNull
import com.google.gson.Gson
import com.iconcreations.balain.data.remote.model.response.BaseResponse
import com.iconcreations.balain.domain.ErrorResponse
import com.iconcreations.balain.domain.Resource
import retrofit2.Response
import javax.inject.Inject

class ResourceMapper<DATA : BaseResponse> @Inject constructor() {

    @NonNull
    fun map (@NonNull response:Response<DATA>) : Resource<DATA?> {
        if (response.code() == 200) {
            return Resource.success(response.body())
        }else if (response.code()!=200){
            val message:String = response.message()
            return Resource.apiError(message, response.code())
        }else {
            val errorMessage: String? = parseErrorResponse(response.errorBody()!!.string()).errorMessage
            return Resource.apiError(errorMessage, response.code())
        }
    }

    private fun parseErrorResponse(json:String) : ErrorResponse {
        var gson = Gson()
        return gson.fromJson(json, ErrorResponse::class.java)
    }
}