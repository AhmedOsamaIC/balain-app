package com.iconcreations.balain.domain.usecase.auth

interface AuthUsecase {

    fun getAuth(): String?
    fun setAuth(auth: String): Boolean
    fun removeAuth(): Boolean
}
