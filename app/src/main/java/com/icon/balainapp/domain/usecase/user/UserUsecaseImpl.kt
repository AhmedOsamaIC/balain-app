package com.iconcreations.balain.domain.usecase.user

import com.iconcreations.balain.data.remote.model.response.User
import com.iconcreations.balain.data.repo.user.UserRepo
import javax.inject.Inject

class UserUsecaseImpl @Inject constructor(private  val userRepo: UserRepo): UserUsecase {


    override fun setUser(user: User) {
        userRepo.setUser(user)
    }

    override fun getUser(): User {
        return userRepo.getUser()
    }

    override fun clearUser(): Boolean {
        return userRepo.clearUser()
    }

    override fun changeLanguage(lang: String): Boolean {
        return userRepo.changeLanguage(lang)
    }
}