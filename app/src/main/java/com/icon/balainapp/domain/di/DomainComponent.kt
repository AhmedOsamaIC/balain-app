package com.iconcreations.balain.domain.di

import com.iconcreations.balain.data.remote.di.DataComponent
import com.iconcreations.balain.domain.usecase.auth.AuthUsecase
import com.iconcreations.balain.domain.usecase.di.UsecaseModule
import com.iconcreations.balain.domain.usecase.user.UserUsecase
import dagger.Component

@Component(modules = [UsecaseModule::class], dependencies = [DataComponent::class])
@DomainScope
interface DomainComponent {

    fun authUsecase(): AuthUsecase
    fun UserUsecase(): UserUsecase
}