package com.iconcreations.balain.data.local.user

import com.iconcreations.balain.data.remote.model.response.User

interface UserStore {

    fun setUser(user: User)
    fun getUser(): User
    fun clearUser(): Boolean
    fun changeLanguage(lang: String): Boolean

}