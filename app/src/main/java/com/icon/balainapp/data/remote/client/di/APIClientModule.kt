package com.iconcreations.balain.data.remote.client.di

import com.iconcreations.balain.data.local.auth.di.AuthStoreModule
import com.iconcreations.balain.data.remote.ApiServices
import com.iconcreations.balain.data.remote.client.SettingsAPI
import com.iconcreations.balain.data.remote.di.DataScope
import dagger.Module
import dagger.Provides
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory

@Module(includes = [AuthStoreModule::class])
object APIClientModule {

    val OK_HTTP_RETROFIT = "okRetrofit"

    val AUTH_INTERCEPTOR_NAME = "AuthInterceptorDaggerName"

    //    @Binds
//    @Named(value = AUTH_INTERCEPTOR_NAME)
//    public abstract Interceptor bindsAuthInterceptor(APIClientAuthInterceptor apiClientAuthInterceptor);
//    @Provides
//    @Named(value = OK_HTTP_RETROFIT)
//    public static OkHttpClient provideOkHttpRetrofit(@Named(AUTH_INTERCEPTOR_NAME) Interceptor authInterceptor) {
//        OkHttpClient.Builder httpClient = new OkHttpClient.Builder();
//        httpClient.readTimeout(SettingsAPI.TIME_OUT, TimeUnit.MILLISECONDS);
//        httpClient.writeTimeout(SettingsAPI.TIME_OUT, TimeUnit.MILLISECONDS);
//
//        HttpLoggingInterceptor logging = new HttpLoggingInterceptor();
//        logging.setLevel(HttpLoggingInterceptor.Level.BODY);
//
//        httpClient.addInterceptor(authInterceptor);
//        httpClient.addInterceptor(logging);
//
//        return httpClient.build();
//    }
//    @Provides
//    @Named(value = OK_HTTP_PICASSO)
//    public static OkHttpClient provideOkHttpPicasso(@Named(AUTH_INTERCEPTOR_NAME) Interceptor authInterceptor) {
//        OkHttpClient.Builder httpClient = new OkHttpClient.Builder();
//        httpClient.readTimeout(SettingsAPI.TIME_OUT, TimeUnit.MILLISECONDS);
//        httpClient.writeTimeout(SettingsAPI.TIME_OUT, TimeUnit.MILLISECONDS);
//
//        HttpLoggingInterceptor logging = new HttpLoggingInterceptor();
//        logging.setLevel(HttpLoggingInterceptor.Level.BODY);
//
//        return httpClient.build();
//    }


    //    @Binds
//    @Named(value = AUTH_INTERCEPTOR_NAME)
//    public abstract Interceptor bindsAuthInterceptor(APIClientAuthInterceptor apiClientAuthInterceptor);
//    @Provides
//    @Named(value = OK_HTTP_RETROFIT)
//    public static OkHttpClient provideOkHttpRetrofit(@Named(AUTH_INTERCEPTOR_NAME) Interceptor authInterceptor) {
//        OkHttpClient.Builder httpClient = new OkHttpClient.Builder();
//        httpClient.readTimeout(SettingsAPI.TIME_OUT, TimeUnit.MILLISECONDS);
//        httpClient.writeTimeout(SettingsAPI.TIME_OUT, TimeUnit.MILLISECONDS);
//
//        HttpLoggingInterceptor logging = new HttpLoggingInterceptor();
//        logging.setLevel(HttpLoggingInterceptor.Level.BODY);
//
//        httpClient.addInterceptor(authInterceptor);
//        httpClient.addInterceptor(logging);
//
//        return httpClient.build();
//    }
//    @Provides
//    @Named(value = OK_HTTP_PICASSO)
//    public static OkHttpClient provideOkHttpPicasso(@Named(AUTH_INTERCEPTOR_NAME) Interceptor authInterceptor) {
//        OkHttpClient.Builder httpClient = new OkHttpClient.Builder();
//        httpClient.readTimeout(SettingsAPI.TIME_OUT, TimeUnit.MILLISECONDS);
//        httpClient.writeTimeout(SettingsAPI.TIME_OUT, TimeUnit.MILLISECONDS);
//
//        HttpLoggingInterceptor logging = new HttpLoggingInterceptor();
//        logging.setLevel(HttpLoggingInterceptor.Level.BODY);
//
//        return httpClient.build();
//    }

    @Provides
    @JvmStatic
    fun provideRetrofit(): Retrofit {
        val client = OkHttpClient.Builder()
            .addNetworkInterceptor(HttpLoggingInterceptor().setLevel(HttpLoggingInterceptor.Level.BODY))
            .build()
        return Retrofit.Builder()
            .baseUrl(SettingsAPI().BASE_URL)
            .addConverterFactory(GsonConverterFactory.create())
            .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
            .client(client)
            .build()
    }

    @DataScope
    @Provides
    @JvmStatic
    fun createApiServices(retrofit: Retrofit): ApiServices {
        return retrofit.create<ApiServices>(ApiServices::class.java)
    }
}