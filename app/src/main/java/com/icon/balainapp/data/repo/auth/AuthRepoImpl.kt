package com.iconcreations.balain.data.repo.auth

import com.iconcreations.balain.data.local.auth.AuthStore
import javax.inject.Inject

class AuthRepoImpl @Inject constructor(private val authStore: AuthStore): AuthRepo {

    override fun getAuth(): String? {
        return authStore.getAuth()
    }

    override fun setAuth(auth: String): Boolean {
        return authStore.setAuth(auth)
    }

    override fun clearAuth(): Boolean {
        return authStore.clearAuth()
    }
}