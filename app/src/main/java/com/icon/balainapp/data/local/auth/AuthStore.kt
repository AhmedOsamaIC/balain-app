package com.iconcreations.balain.data.local.auth

interface AuthStore {

    fun getAuth(): String?
    fun setAuth(auth: String): Boolean
    fun clearAuth(): Boolean
}