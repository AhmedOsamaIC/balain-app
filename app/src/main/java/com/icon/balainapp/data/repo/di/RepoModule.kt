package com.iconcreations.balain.data.repo.di

import com.iconcreations.balain.data.local.user.di.UserStoreModule
import com.iconcreations.balain.data.remote.client.di.APIClientModule
import com.iconcreations.balain.data.repo.auth.AuthRepo
import com.iconcreations.balain.data.repo.auth.AuthRepoImpl
import com.iconcreations.balain.data.repo.user.UserRepo
import com.iconcreations.balain.data.repo.user.UserRepoImpl
import dagger.Binds
import dagger.Module

@Module(includes = [APIClientModule::class, UserStoreModule::class])
interface RepoModule {

    @Binds
    fun bindsAuthRepo(authRepoImpl: AuthRepoImpl): AuthRepo

    @Binds
    fun bindsUserRepo(userRepoImpl: UserRepoImpl): UserRepo
}