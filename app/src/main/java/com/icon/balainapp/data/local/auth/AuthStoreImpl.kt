package com.iconcreations.balain.data.local.auth

import android.content.SharedPreferences
import com.iconcreations.balain.data.local.auth.di.AuthStoreModule.Companion.AUTH_SHARED_PREFERENCE_DAGGER_NAME
import javax.inject.Inject
import javax.inject.Named

class AuthStoreImpl @Inject constructor(
    @Named(value = AUTH_SHARED_PREFERENCE_DAGGER_NAME)var sharedPreference: SharedPreferences)
    : AuthStore {

    private val TOKEN: String = "token"

    override fun getAuth(): String? {
        return sharedPreference.getString(TOKEN, null)
    }

    override fun setAuth(auth: String): Boolean {
        return sharedPreference.edit().putString(TOKEN, auth).commit()
    }

    override fun clearAuth(): Boolean {
        return sharedPreference.edit().clear().commit()
    }
}