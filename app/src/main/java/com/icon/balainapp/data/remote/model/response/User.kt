package com.iconcreations.balain.data.remote.model.response

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

class User {

    var lang: String? = null
    @SerializedName("id")
    @Expose
    var id: Int? = null
    @SerializedName("userName")
    @Expose
    var userName: String? = null
    @SerializedName("email")
    @Expose
    var email: String? = null
    @SerializedName("mobileNumber")
    @Expose
    var mobileNumber: String? = null
    @SerializedName("token")
    @Expose
    var token: String? = null
    @SerializedName("fcmToken")
    @Expose
    var fcmToken: String? = null
    @SerializedName("firstLogin")
    @Expose
    var firstLogin: Boolean? = null

    constructor(lang: String?, id: Int?, userName: String?, email: String?, mobileNumber: String?,
                token: String?, fcmToken: String?, firstLogin: Boolean?) {
        this.lang = lang
        this.id = id
        this.userName = userName
        this.email = email
        this.mobileNumber = mobileNumber
        this.token = token
        this.fcmToken = fcmToken
        this.firstLogin = firstLogin
    }
    constructor()

}