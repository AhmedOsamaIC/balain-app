package com.iconcreations.balain.data.remote.di

import javax.inject.Scope

@Scope
annotation class DataScope