package com.iconcreations.balain.data.remote.di

import android.content.Context
import com.iconcreations.balain.data.repo.auth.AuthRepo
import com.iconcreations.balain.data.repo.di.RepoModule
import com.iconcreations.balain.data.repo.user.UserRepo
import dagger.BindsInstance
import dagger.Component

@DataScope
@Component(modules = [RepoModule::class])
interface DataComponent {

    fun authRepo(): AuthRepo
    fun userRepo(): UserRepo

    @Component.Builder
    interface Builder {
        @BindsInstance
        fun bindContext(context: Context): Builder
        fun build(): DataComponent
    }
}