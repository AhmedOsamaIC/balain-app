package com.iconcreations.balain.data.local.user

import android.content.SharedPreferences
import com.iconcreations.balain.data.local.user.di.UserStoreModule.Companion.USER_SHARED_PREFERENCE_DAGGER_NAME
import com.iconcreations.balain.data.remote.model.response.User
import javax.inject.Inject
import javax.inject.Named

class UserStoreImpl @Inject constructor
    (@Named (value = USER_SHARED_PREFERENCE_DAGGER_NAME) var sharedPreference: SharedPreferences)
    : UserStore {

    private val USER_NAME = "fName"
    private val USER_ID = "userId"
    private val EMAIL_KEY = "email"
    private val USER_MOBILE_NUMBER = "phone"
    private val FIRST_LOGIN = "isFirstLogin"
    private val ROLE = "role"
    private val ADD_DATE = "addDate"
    private val LANG = "language"
    private val IMAGE_PATH_KEY = "imgPath"

    override fun setUser(user: User) {
        val editor: SharedPreferences.Editor = sharedPreference.edit()
        editor.putString(USER_NAME, user.userName)
    }

    override fun getUser(): User {
        val userName: String?= sharedPreference.getString(USER_NAME, null)
        return User(userName, null, null, null, null,
            null, null, null)
    }

    override fun clearUser(): Boolean {
        return false
    }

    override fun changeLanguage(lang: String): Boolean {
        return false
    }
}