package com.iconcreations.balain.data.local.user.di

import android.content.Context
import android.content.SharedPreferences
import com.iconcreations.balain.data.local.user.UserStore
import com.iconcreations.balain.data.local.user.UserStoreImpl
import dagger.Binds
import dagger.Module
import dagger.Provides
import javax.inject.Named

@Module
abstract class UserStoreModule {

    @Module
    companion object{
        const val USER_SHARED_PREFERENCE_DAGGER_NAME: String = "userPreferenceDaggerName"
        const val USER_SHARED_PREFERENCE_NAME: String = "userPreferenceDaggerName"

        @Provides
        @JvmStatic
        @Named(value = USER_SHARED_PREFERENCE_DAGGER_NAME)
        fun getUserSharedPreferece(context: Context): SharedPreferences{
            return context.getSharedPreferences(USER_SHARED_PREFERENCE_NAME, Context.MODE_PRIVATE)
        }
    }

    @Binds
    abstract fun bindsUserStore(userStore: UserStoreImpl): UserStore
}