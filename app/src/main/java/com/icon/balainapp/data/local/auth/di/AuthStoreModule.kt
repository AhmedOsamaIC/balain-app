package com.iconcreations.balain.data.local.auth.di

import android.content.Context
import android.content.SharedPreferences
import com.iconcreations.balain.data.local.auth.AuthStore
import com.iconcreations.balain.data.local.auth.AuthStoreImpl
import dagger.Binds
import dagger.Module
import dagger.Provides
import javax.inject.Named

@Module
abstract class AuthStoreModule {

    @Module
    companion object{
        const val AUTH_SHARED_PREFERENCE_DAGGER_NAME: String = "authPreferenceDaggerName"
        const val AUTH_SHARED_PREFERENCE_NAME: String = "authPreferenceDaggerName"

        @Provides
        @JvmStatic
        @Named(value = AUTH_SHARED_PREFERENCE_DAGGER_NAME)
        fun provideAuthSharedPreference(context: Context): SharedPreferences {
            return context.getSharedPreferences(AUTH_SHARED_PREFERENCE_NAME, Context.MODE_PRIVATE)
        }
    }

    @Binds
    abstract fun bindsAuthStore(authStore: AuthStoreImpl): AuthStore
}