package com.iconcreations.balain.data.repo.user

import com.iconcreations.balain.data.local.user.UserStore
import com.iconcreations.balain.data.remote.model.response.User
import javax.inject.Inject

class UserRepoImpl @Inject constructor(private val userStore: UserStore): UserRepo {

    override fun setUser(user: User) {
        userStore.setUser(user)
    }

    override fun getUser(): User {
        return userStore.getUser()
    }

    override fun clearUser(): Boolean {
        return userStore.clearUser()
    }

    override fun changeLanguage(lang: String): Boolean {
        return userStore.changeLanguage(lang)
    }

}