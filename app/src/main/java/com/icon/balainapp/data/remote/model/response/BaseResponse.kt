package com.iconcreations.balain.data.remote.model.response

import com.google.gson.annotations.SerializedName

open class BaseResponse{

    @SerializedName("code")
    var code: Int? = null

    @SerializedName("msg")
    var message: String? = null

    @SerializedName("sameToken")
    var sameToken: Boolean = false
}

