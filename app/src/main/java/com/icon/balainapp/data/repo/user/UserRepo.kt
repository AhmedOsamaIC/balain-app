package com.iconcreations.balain.data.repo.user

import com.iconcreations.balain.data.remote.model.response.User

interface UserRepo {

    fun setUser(user: User)
    fun getUser(): User
    fun clearUser(): Boolean
    fun changeLanguage(lang: String): Boolean
}