package com.iconcreations.balain.data.repo.auth

interface AuthRepo {

    fun getAuth(): String?
    fun setAuth(auth: String): Boolean
    fun clearAuth(): Boolean
}