package com.icon.balainapp

import android.app.Activity
import android.content.Context
import com.iconcreations.balain.data.remote.di.DaggerDataComponent
import com.iconcreations.balain.data.remote.di.DataComponent
import com.iconcreations.balain.domain.di.DaggerDomainComponent
import com.iconcreations.balain.domain.di.DomainComponent
import com.iconcreations.balain.presentaion.di.DaggerPresentationComponent
import com.iconcreations.balain.presentaion.di.PresentationComponent
import dagger.android.AndroidInjector
import dagger.android.DaggerApplication

abstract class BalainApp: DaggerApplication() {

    private lateinit var context: Context
    private lateinit var presentationComponent: PresentationComponent
    private lateinit var dataComponent: DataComponent
    private lateinit var domainComponent: DomainComponent
    private val userPreferenceDaggerName = "userPreferenceDaggerName"

    override fun onCreate() {
        super.onCreate()
        context = applicationContext

        dataComponent = DaggerDataComponent.builder().bindContext(this)
            .build()
        domainComponent = DaggerDomainComponent.builder()
            .dataComponent(dataComponent).build()
        presentationComponent = DaggerPresentationComponent.builder()
            .domainComponent(domainComponent).build()
    }

//    override fun attachBaseContext(base: Context?) {
//        val language: String = LocaleUtils.getLanguage(base)
//        super.attachBaseContext(LocaleUtils.onAttach(base, language))
//    }

    override fun applicationInjector(): AndroidInjector<out DaggerApplication> {
        dataComponent = DaggerDataComponent.builder().bindContext(this)
            .build()
        domainComponent = DaggerDomainComponent.builder().dataComponent(dataComponent).build()
        presentationComponent = DaggerPresentationComponent.builder().domainComponent(domainComponent).build()
        presentationComponent.inject(this)
        return presentationComponent
    }

    fun getInstance(activity: Activity): BalainApp {
        return activity.application as BalainApp
    }

    fun getPresentationComponent(): PresentationComponent? {
        return presentationComponent
    }
//
    fun getDataComponent(): DataComponent? {
        return dataComponent
    }

    fun getDomainComponent(): DomainComponent? {
        return domainComponent
    }

    fun getAppContext(): Context {
        return context
    }
}