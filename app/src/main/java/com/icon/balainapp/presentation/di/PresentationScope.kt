package com.iconcreations.balain.presentaion.di

import javax.inject.Scope

@Scope
annotation class PresentationScope
