package com.iconcreations.balain.presentaion.di

import com.icon.balainapp.BalainApp
import com.iconcreations.balain.domain.di.DomainComponent
import com.iconcreations.balain.presentaion.viewmodel_factory.ViewModelFactory
import com.iconcreations.balain.presentaion.viewmodel_factory.di.ViewModelFactoryModule
import dagger.Component
import dagger.android.AndroidInjectionModule
import dagger.android.AndroidInjector
import dagger.android.DaggerApplication

@Component(modules = [ViewModelFactoryModule::class, AndroidInjectionModule::class],
    dependencies = [DomainComponent::class])
@PresentationScope
interface PresentationComponent: AndroidInjector<DaggerApplication> {

    fun viewModelFactory(): ViewModelFactory
    fun inject(app: BalainApp)
}
