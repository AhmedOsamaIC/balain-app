package com.iconcreations.balain.presentaion.viewmodel.di

import androidx.lifecycle.ViewModel
import com.icon.balainapp.presentation.viewmodel.splash.SplashViewModel
import com.icon.balainapp.presentation.viewmodel_factory.di.ViewModelKey
import dagger.Binds
import dagger.Module
import dagger.multibindings.IntoMap

@Module
interface ViewModelsModule {

    @Binds
    @IntoMap
    @ViewModelKey(SplashViewModel::class)
    fun bindsLoginViewModel(loginViewModel: SplashViewModel): ViewModel
}