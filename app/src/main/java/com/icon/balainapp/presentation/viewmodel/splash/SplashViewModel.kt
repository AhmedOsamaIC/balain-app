package com.icon.balainapp.presentation.viewmodel.splash

import com.iconcreations.balain.presentaion.viewmodel.BaseViewModel
import com.iconcreations.balain.presentaion.viewmodel_factory.di.SchedulersModule
import io.reactivex.Scheduler
import javax.inject.Inject
import javax.inject.Named

class SplashViewModel @Inject constructor(@Named(SchedulersModule.IO_SCHEDULER) val subscribeOn: Scheduler,
                                          @Named(SchedulersModule.MAIN_THREAD_SCHEDULER) val observeOn: Scheduler)
    : BaseViewModel(subscribeOn, observeOn) {
}