package com.iconcreations.balain.presentaion.viewmodel_factory.di

import androidx.lifecycle.ViewModelProvider
import com.iconcreations.balain.presentaion.di.PresentationScope
import com.iconcreations.balain.presentaion.viewmodel.di.ViewModelsModule
import com.iconcreations.balain.presentaion.viewmodel_factory.ViewModelFactory
import dagger.Binds
import dagger.Module

@Module(includes = [ViewModelsModule::class, SchedulersModule::class])
interface ViewModelFactoryModule {

    @Binds
    @PresentationScope
    fun provideViewModelFactory(viewModelFactory: ViewModelFactory): ViewModelProvider.Factory
}
